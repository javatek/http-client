package org.bitbucket.javatek.http.client;

import com.sun.net.httpserver.HttpExchange;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.StringJoiner;

import static java.text.MessageFormat.format;
import static org.junit.Assert.assertEquals;

public class HttpClientTest {
  private final TestServer server = new TestServer();
  private final HttpClient client = new HttpClient();

  @Before
  public void before() {
    server.start();
  }

  @After
  public void after() {
    server.stop();
  }

  @Test
  public void testGetOK() throws IOException {
    server.handle("/ok",
      httpExchange -> new HttpOK("Hello, World!"));

    HttpResponse response = client.get(server.url.withPath("/ok"));

    assertEquals(200, response.statusCode());
    assertEquals("OK", response.responseMessage());
    assertEquals("Hello, World!", response.text());
  }

  @Test
  public void testGetBadRequest() throws IOException {
    server.handle("/bad-request",
      httpExchange -> new HttpBadRequest("Bad request content"));

    HttpResponse response = client.get(server.url.withPath("/bad-request"));

    assertEquals(400, response.statusCode());
    assertEquals("Bad Request", response.responseMessage());
    assertEquals("Bad request content", response.text());
  }

  @Test
  public void testPostOK() throws IOException {
    String expected = "POST content";

    server.handle("/ok", httpExchange -> {
      String requestBody = readRequestBody(httpExchange);
      if (!expected.equals(requestBody)) {
        return new HttpBadRequest(
          format("Invalid request content. " +
            "Expected is '{0}' but actual is '{1}'", expected, requestBody)
        );
      }
      return new HttpOK(requestBody);
    });

    HttpResponse response = client.post(server.url.withPath("/ok"), expected);

    assertEquals(expected, response.text());
    assertEquals(200, response.statusCode());
    assertEquals("OK", response.responseMessage());
  }

  private String readRequestBody(HttpExchange httpExchange) throws IOException {
    try (InputStream is = httpExchange.getRequestBody()) {
      try (Reader r = new InputStreamReader(is)) {
        try (BufferedReader br = new BufferedReader(r)) {
          StringJoiner joiner = new StringJoiner("\n");

          String line;
          while ((line = br.readLine()) != null)
            joiner.add(line);

          return joiner.toString();
        }
      }
    }
  }

  @Test(expected = IOException.class)
  public void testBrokenConnection() throws IOException {
    server.handle("/broken-connection",
      httpExchange -> {throw new IOException("Connection is broken!");});

    client.get(server.url.withPath("/broken-connection"));
  }

  @Test
  public void testReceiveCookies() throws IOException {
    server.handle("/cookies",
      httpExchange ->
        new HttpOK(
          "OK",
          new Cookies()
            .with("cookieEn", "Value with spaces")
            .with("cookieRu", "\"Значение русс\"")
        )
    );

    HttpResponse response = client.get(server.url.withPath("/cookies"));
    Cookies cookies = response.cookies();
    assertEquals("Value with spaces", cookies.get("cookieEn"));
    assertEquals("\"Значение русс\"", cookies.get("cookieRu"));
  }
}