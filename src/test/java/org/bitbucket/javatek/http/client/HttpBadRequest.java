package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;

/**
 *
 */
final class HttpBadRequest implements HttpResponse {
  private final Content content;

  HttpBadRequest(String content) {
    this.content = new Content("plain/text", content);
  }

  @Override
  public int statusCode() {
    return 400;
  }

  @Override
  public String responseMessage() {
    return "Bad Request";
  }

  @Nonnull
  @Override
  public Content content() {
    return content;
  }
}
