package org.bitbucket.javatek.http.client;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpServer;
import org.bitbucket.javatek.url.URL;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetSocketAddress;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 *
 */
final class TestServer {
  final int port = 8080;
  final URL url = new URL("http://localhost:" + port);

  private final HttpServer server;

  TestServer() {
    try {
      server = HttpServer.create(new InetSocketAddress(port), 0);
    }
    catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  void start() {
    server.start();
  }

  void stop() {
    server.stop(0);
  }

  void handle(String context, Handler handler) {
    server.createContext(context, httpExchange ->
    {
      HttpResponse response = handler.handle(httpExchange);
      Content content = response.content();

      Headers responseHeaders = httpExchange.getResponseHeaders();
      responseHeaders.add("Content-Type", content.type() + "; charset=UTF-8");
      for (Cookie cookie : response.cookies()) {
        responseHeaders.add("Set-Cookie", cookie.toHeader());
      }

      httpExchange.sendResponseHeaders(response.statusCode(), content.length());
      try (OutputStream out = httpExchange.getResponseBody()) {
        try (Writer writer = new OutputStreamWriter(out, UTF_8)) {
          writer.write(content.text());
        }
      }
    });
  }

  @FunctionalInterface
  interface Handler {
    HttpResponse handle(HttpExchange httpExchange) throws IOException;
  }
}
