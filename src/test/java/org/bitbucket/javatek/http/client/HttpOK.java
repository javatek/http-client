package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;

import static org.bitbucket.javatek.http.client.Cookies.NO_COOKIES;

/**
 *
 */
final class HttpOK implements HttpResponse {
  private final Content content;
  private final Cookies cookies;

  HttpOK(String content, Cookies cookies) {
    this.content = new Content("plain/text", content);
    this.cookies = cookies;
  }

  HttpOK(String body) {
    this(body, NO_COOKIES);
  }

  @Override
  public int statusCode() {
    return 200;
  }

  @Override
  public String responseMessage() {
    return "OK";
  }

  @Nonnull
  @Override
  public Cookies cookies() {
    return cookies;
  }

  @Nonnull
  @Override
  public Content content() {
    return content;
  }
}
