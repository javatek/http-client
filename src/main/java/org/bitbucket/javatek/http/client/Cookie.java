package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;
import static org.bitbucket.javatek.require.StringRequires.requireNonEmpty;

/**
 *
 */
public final class Cookie {
  @Nonnull
  public final String name;

  @Nonnull
  public final String value;

  Cookie(@Nonnull String name, @Nonnull String value) {
    this.name = requireNonEmpty(name);
    this.value = requireNonNull(value);
  }

  Cookie(@Nonnull String header) {
    int i = 0;
    StringBuilder name = new StringBuilder();
    for (; i < header.length(); i++) {
      char c = header.charAt(i);
      if (c == '=')
        break;
      name.append(c);
    }

    i++;
    StringBuilder value = new StringBuilder();
    for (; i < header.length(); i++) {
      char c = header.charAt(i);
      if (c == ';')
        break;
      value.append(c);
    }

    try {
      this.name = name.toString();
      this.value = URLDecoder.decode(value.toString(), "UTF-8");
    }
    catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
  }

  String toHeader() {
    try {
      return name + "=" + URLEncoder.encode(value, "UTF-8");
    }
    catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public String toString() {
    return name + ": " + value;
  }
}
