package org.bitbucket.javatek.http.client;

import org.bitbucket.javatek.url.URL;

import javax.annotation.Nonnull;

import static org.bitbucket.javatek.http.client.Cookies.NO_COOKIES;

/**
 *
 */
public interface HttpRequest {
  @Nonnull
  URL url();

  @Nonnull
  String method();

  @Nonnull
  default Cookies cookies() {
    return NO_COOKIES;
  }

  @Nonnull
  default String contentType() {
    return "plain/text";
  }

  @Nonnull
  default String content() {
    return "";
  }
}
