package org.bitbucket.javatek.http.client;

import org.bitbucket.javatek.url.URL;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URLConnection;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import static java.lang.Math.toIntExact;
import static org.bitbucket.javatek.http.client.Headers.NO_HEADERS;

/**
 *
 */
final class Connection implements AutoCloseable {
  private final HttpURLConnection connection;

  Connection(@Nonnull URL url) throws IOException {
    URLConnection connection = url.openConnection();
    if (!(connection instanceof HttpURLConnection)) {
      throw new IllegalArgumentException(
        "Request url is not HTTP: " + url
      );
    }
    this.connection = (HttpURLConnection) connection;
  }

  void setRequestMethod(String method) throws ProtocolException {
    connection.setRequestMethod(method);
  }

  void setKeepAlive(boolean keepAlive) {
    if (keepAlive) {
      connection.setRequestProperty("Connection", "keep-alive");
    }
  }

  void setTimeout(Duration timeout) {
    int millis = toIntExact(timeout.toMillis());
    connection.setConnectTimeout(millis);
    connection.setReadTimeout(millis);
  }

  void setCharset(String charset) {
    connection.setRequestProperty("Accept-Charset", charset);
  }

  @Nullable
  String getCharset() {
    return connection.getContentEncoding();
  }

  @Nonnull
  String getContentType() {
    String contentType = connection.getContentType();
    return
      contentType != null
        ? contentType
        : "plain/text";
  }

  void setContentType(String contentType) {
    connection.setRequestProperty("Content-Type", contentType);
  }

  void setContentLength(int length) {
    connection.setRequestProperty("Content-Length", String.valueOf(length));
    connection.setDoOutput(length > 0);
  }

  void addCookie(Cookie cookie) {
    connection.addRequestProperty("Cookie", cookie.toHeader());
  }

  void connect() throws IOException {
    connection.connect();
  }

  void write(String content, String charset) throws IOException {
    if (content.length() > 0) {
      try (OutputStream os = connection.getOutputStream()) {
        try (Writer w = new OutputStreamWriter(os, charset)) {
          try (BufferedWriter bw = new BufferedWriter(w)) {
            bw.write(content);
          }
        }
      }
    }
  }

  int getResponseCode() throws IOException {
    return connection.getResponseCode();
  }

  String getResponseMessage() throws IOException {
    return connection.getResponseMessage();
  }

  Headers getHeaders() {
    Headers headers = NO_HEADERS;
    Map<String, List<String>> headerFields = connection.getHeaderFields();
    for (Map.Entry<String, List<String>> entry : headerFields.entrySet()) {
      if (entry.getKey() != null) {
        headers = headers.with(
          new Header(
            entry.getKey(),
            entry.getValue()
          )
        );
      }
    }
    return headers;
  }

  String read(String charset) throws IOException {
    try (InputStream is = connection.getInputStream()) {
      return readStream(is, charset);
    }
    catch (IOException e) {
      try (InputStream is = connection.getErrorStream()) {
        return is != null
          ? readStream(is, charset)
          : "";
      }
      catch (IOException | RuntimeException e2) {
        e.addSuppressed(e2);
        throw e;
      }
    }
  }

  private String readStream(InputStream stream, String charset) throws IOException {
    try (Reader r = new InputStreamReader(stream, charset)) {
      try (BufferedReader br = new BufferedReader(r)) {
        StringJoiner joiner = new StringJoiner("\n");

        String line;
        while ((line = br.readLine()) != null)
          joiner.add(line);

        return joiner.toString();
      }
    }
  }

  @Override
  public void close() {
    clearInputStream();
    clearErrorStream();

    String conn = connection.getHeaderField("Connection");
    if ("close".equalsIgnoreCase(conn)) {
      connection.disconnect();
    }
  }

  private void clearInputStream() {
    try (InputStream stream = connection.getInputStream()) {
      if (stream != null) {
        //noinspection StatementWithEmptyBody
        while (stream.read() != -1)
          ;
      }
    }
    catch (IOException | RuntimeException ignored) {}
  }

  private void clearErrorStream() {
    try (InputStream stream = connection.getErrorStream()) {
      if (stream != null) {
        //noinspection StatementWithEmptyBody
        while (stream.read() != -1)
          ;
      }
    }
    catch (IOException | RuntimeException ignored) {}
  }
}
