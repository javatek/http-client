package org.bitbucket.javatek.http.client;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 *
 */
interface Constants {
  /**
   * Encoding for all requests
   */
  String CHARSET = UTF_8.name();

  boolean KEEP_ALIVE = true;
}
