package org.bitbucket.javatek.http.client;

import org.bitbucket.javatek.url.URL;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;

import static java.text.MessageFormat.format;
import static org.bitbucket.javatek.http.client.Constants.CHARSET;
import static org.bitbucket.javatek.http.client.Constants.KEEP_ALIVE;
import static org.bitbucket.javatek.http.client.Cookies.NO_COOKIES;
import static org.bitbucket.javatek.http.client.HttpOptions.DEFAULT_OPTIONS;
import static org.bitbucket.javatek.require.NumberRequires.requirePositive;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 * HTTP Client over JDK standard HttpURLConnection with blocking IO
 */
public final class HttpClient {
  private final HttpOptions options;

  /**
   * Basic ctor
   * @param options options for whole client
   */
  public HttpClient(@Nonnull HttpOptions options) {
    this.options = requireNonNull(options);
    requirePositive(options.timeout());
  }

  /**
   * Default ctor
   */
  public HttpClient() {
    this(DEFAULT_OPTIONS);
  }

  /**
   * Performs HTTP GET
   * @param url target for request
   * @return HTTP response
   * @throws IOException an I/O error in case of network (socket) problem
   */
  @Nonnull
  public HttpResponse get(@Nonnull String url) throws IOException {
    return service(
      new HttpGet(
        new URL(url)
      )
    );
  }

  /**
   * Performs HTTP GET
   * @param url target for request
   * @return HTTP response
   * @throws IOException an I/O error in case of network (socket) problem
   */
  @Nonnull
  public HttpResponse get(@Nonnull URL url) throws IOException {
    return service(
      new HttpGet(url)
    );
  }

  /**
   * Performs HTTP GET
   * @param url     target for request
   * @param cookies cookies for request
   * @return HTTP response
   * @throws IOException an I/O error in case of network (socket) problem
   */
  @Nonnull
  public HttpResponse get(@Nonnull URL url, @Nonnull Cookies cookies) throws IOException {
    return service(
      new HttpGet(url, cookies)
    );
  }

  public HttpResponse post(URL url, String plainText) throws IOException {
    return service(
      new HttpPost(url, plainText)
    );
  }

  public HttpResponse post(URL url, FormData formData) throws IOException {
    return service(
      new HttpPost(url, formData)
    );
  }

  //////////////////////////////////////////////////////////////////////////////

  @Nonnull
  public HttpResponse service(@Nonnull HttpRequest request) throws IOException {
    try (Connection connection = new Connection(request.url())) {
      connection.setRequestMethod(request.method());
      connection.setKeepAlive(KEEP_ALIVE);
      connection.setTimeout(options.timeout());
      connection.setCharset(CHARSET);
      connection.setContentType(request.contentType());
      connection.setContentLength(computeContentLength(request));

      for (Cookie cookie : request.cookies()) {
        connection.addCookie(cookie);
      }

      connection.connect();

      connection.write(request.content(), CHARSET);

      int responseCode = connection.getResponseCode();
      String responseMessage = connection.getResponseMessage();
      Headers responseHeaders = connection.getHeaders();
      Cookies responseCookies = makeResponseCookies(responseHeaders);
      String responseCharset =
        connection.getCharset() != null
          ? connection.getCharset()
          : CHARSET;
      Content responseContent =
        new Content(
          connection.getContentType(),
          connection.read(responseCharset)
        );

      return new HttpResponse() {
        @Override
        public int statusCode() {
          return responseCode;
        }

        @Nullable
        @Override
        public String responseMessage() {
          return responseMessage;
        }

        @Nonnull
        @Override
        public Headers headers() {
          return responseHeaders;
        }

        @Nonnull
        @Override
        public Cookies cookies() {
          return responseCookies;
        }

        @Nonnull
        @Override
        public Content content() {
          return responseContent;
        }
      };
    }
    catch (IOException e) {
      throw new IOException(
        format("{0} {1} failed", request.method(), request.url()),
        e
      );
    }

  }

  private Cookies makeResponseCookies(Headers headers) {
    Header header = headers.get("Set-Cookie");
    if (header != null) {
      Cookies cookies = NO_COOKIES;
      for (String value : header.values()) {
        cookies = cookies.with(new Cookie(value));
      }
      return cookies;
    }
    return NO_COOKIES;
  }

  private int computeContentLength(HttpRequest requestBody) throws IOException {
    String body = requestBody.content();
    byte[] bytes = body.getBytes(CHARSET);
    return bytes.length;
  }
}
