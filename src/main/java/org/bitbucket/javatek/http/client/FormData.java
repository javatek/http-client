package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringJoiner;

import static java.util.Collections.emptyMap;
import static org.bitbucket.javatek.http.client.Constants.CHARSET;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 * todo rename to HttpForm
 */
public final class FormData implements Iterable<FormField> {
  private final Map<String, FormField> fields;

  private FormData(Map<String, FormField> fields) {
    this.fields = requireNonNull(fields);
  }

  public FormData() {
    this(emptyMap());
  }

  @Nonnull
  public FormData with(@Nonnull FormField field) {
    Map<String, FormField> newFields = new LinkedHashMap<>(fields);
    newFields.put(field.name(), field);
    return new FormData(newFields);
  }

  @Nonnull
  public FormData with(@Nonnull String name, String value) {
    return with(new FormField(name, value));
  }

  @Nonnull
  @Override
  public Iterator<FormField> iterator() {
    return fields.values().iterator();
  }

  @Override
  public String toString() {
    StringJoiner joiner = new StringJoiner(", ", "{", "}");
    for (FormField field : this)
      joiner.add(field.toString());
    return joiner.toString();
  }

  public String toUrlEncoded() {
    try {
      StringJoiner joiner = new StringJoiner("&");
      for (FormField field : this) {
        String name = URLEncoder.encode(field.name(), CHARSET);
        String value = URLEncoder.encode(field.value(), CHARSET);
        joiner.add(name + "=" + value);
      }
      return joiner.toString();
    }
    catch (UnsupportedEncodingException e) {
      throw new RuntimeException(e);
    }
  }
}
