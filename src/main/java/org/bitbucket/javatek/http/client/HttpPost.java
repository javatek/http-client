package org.bitbucket.javatek.http.client;

import org.bitbucket.javatek.url.URL;

import javax.annotation.Nonnull;

import static org.bitbucket.javatek.http.client.Cookies.NO_COOKIES;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;
import static org.bitbucket.javatek.require.StringRequires.requireNonEmpty;

/**
 *
 */
public final class HttpPost implements HttpRequest {
  private final URL url;
  private final String content;
  private final String contentType;
  private final Cookies cookies;

  private HttpPost(URL url, String content, String contentType, Cookies cookies) {
    this.url = requireNonNull(url);
    this.content = requireNonNull(content);
    this.contentType = requireNonEmpty(contentType);
    this.cookies = requireNonNull(cookies);
  }

  public HttpPost(URL url, FormData formData) {
    this(
      url,
      formData.toUrlEncoded(),
      "application/x-www-form-urlencoded",
      NO_COOKIES
    );
  }

  public HttpPost(URL url, String plainText) {
    this(url, plainText, "plain/text", NO_COOKIES);
  }

  public HttpPost(URL url) {
    this(url, "");
  }

  public HttpPost withCookies(Cookies cookies) {
    return new HttpPost(url, content, contentType, cookies);
  }

  //////////////////////////////////////////////////////////////////////////////

  @Nonnull
  @Override
  public URL url() {
    return url;
  }

  @Nonnull
  @Override
  public String method() {
    return "POST";
  }

  @Nonnull
  @Override
  public Cookies cookies() {
    return cookies;
  }

  @Nonnull
  @Override
  public String contentType() {
    return contentType;
  }

  @Nonnull
  @Override
  public String content() {
    return content;
  }
}
