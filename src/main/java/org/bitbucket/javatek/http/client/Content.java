package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;

/**
 *
 */
public final class Content {
  private final String type;
  private final String text;

  public Content(String type, String text) {
    this.type = type;
    this.text = text;
  }

  @Nonnull
  public String type() {
    return type;
  }

  public int length() {
    return text.length();
  }

  @Nonnull
  public String text() {
    return text;
  }
}
