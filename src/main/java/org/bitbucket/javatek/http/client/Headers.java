package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.Collections.emptyMap;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 *
 */
public final class Headers implements Iterable<Header> {
  public static final Headers NO_HEADERS = new Headers();

  private final Map<String, Header> headers;

  private Headers(Map<String, Header> headers) {
    this.headers = requireNonNull(headers);
  }

  public Headers() {
    this(emptyMap());
  }

  @Nonnull
  public Headers with(@Nonnull String name, @Nullable String value) {
    Header header = get(name);

    Header newHeader =
      header != null
        ? header.with(value)
        : new Header(name, value);

    return
      header != newHeader
        ? with(newHeader)
        : this;
  }

  @Nonnull
  public Headers with(Header header) {
    Map<String, Header> newHeaders = new LinkedHashMap<>(headers);
    newHeaders.put(header.name().toLowerCase(), header);
    return new Headers(newHeaders);
  }

  @Nullable
  public Header get(@Nonnull String name) {
    return headers.get(name.toLowerCase());
  }

  @Nonnull
  @Override
  public Iterator<Header> iterator() {
    return headers.values().iterator();
  }
}
