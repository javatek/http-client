package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringJoiner;

import static java.util.Collections.emptyMap;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 *
 */
public final class Cookies implements Iterable<Cookie> {
  public static final Cookies NO_COOKIES = new Cookies();

  private final Map<String, Cookie> cookies;

  private Cookies(Map<String, Cookie> cookies) {
    this.cookies = requireNonNull(cookies);
  }

  public Cookies() {
    this(emptyMap());
  }

  @Nonnull
  public Cookies with(@Nonnull String name, @Nonnull String value) {
    return with(new Cookie(name, value));
  }

  @Nonnull
  public Cookies with(Cookie cookie) {
    Map<String, Cookie> newCookies = new LinkedHashMap<>(cookies);
    newCookies.put(cookie.name, cookie);
    return new Cookies(newCookies);
  }

  @Nullable
  public String get(String name) {
    Cookie cookie = cookies.get(name);
    return cookie != null ? cookie.value : null;
  }

  @Nonnull
  @Override
  public Iterator<Cookie> iterator() {
    return cookies.values().iterator();
  }

  @Override
  public String toString() {
    StringJoiner joiner = new StringJoiner(", ", "{", "}");
    for (Cookie cookie : this)
      joiner.add(cookie.toString());
    return joiner.toString();
  }
}
