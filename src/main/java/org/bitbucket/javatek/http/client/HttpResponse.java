package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.bitbucket.javatek.http.client.Cookies.NO_COOKIES;
import static org.bitbucket.javatek.http.client.Headers.NO_HEADERS;

/**
 * todo remove headers?
 */
public interface HttpResponse {
  int statusCode();

  @Nullable
  String responseMessage();

  @Nonnull
  default Headers headers() {
    return NO_HEADERS;
  }

  @Nonnull
  default Cookies cookies() {
    return NO_COOKIES;
  }

  @Nonnull
  Content content();

  @Nonnull
  default String text() throws IllegalStateException {
    return content().text();
  }
}
