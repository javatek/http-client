package org.bitbucket.javatek.http.client;

import org.bitbucket.javatek.url.URL;

import javax.annotation.Nonnull;

import static org.bitbucket.javatek.http.client.Cookies.NO_COOKIES;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 *
 */
public final class HttpGet implements HttpRequest {
  private final URL url;
  private final Cookies cookies;

  public HttpGet(URL url, Cookies cookies) {
    this.url = requireNonNull(url);
    this.cookies = requireNonNull(cookies);
  }

  public HttpGet(URL url) {
    this(url, NO_COOKIES);
  }

  //////////////////////////////////////////////////////////////////////////////

  @Nonnull
  @Override
  public URL url() {
    return url;
  }

  @Nonnull
  @Override
  public String method() {
    return "GET";
  }

  @Nonnull
  @Override
  public Cookies cookies() {
    return cookies;
  }
}
