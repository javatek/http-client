package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.bitbucket.javatek.require.StringRequires.requireNonEmpty;

/**
 *
 */
public final class Header {
  private final String name;
  private final List<String> values;

  public Header(@Nonnull String name, List<String> values) {
    this.name = requireNonEmpty(name);
    this.values = values != null ? values : emptyList();
  }

  public Header(@Nonnull String name, @Nullable String value) {
    this(
      name,
      value != null && !value.isEmpty()
        ? singletonList(value)
        : emptyList()
    );
  }

  @Nonnull
  public Header with(@Nullable String value) {
    if (value != null && !value.isEmpty()) {
      List<String> newValues = new ArrayList<>(values);
      newValues.add(value);
      return new Header(name, newValues);
    }
    return this;
  }

  @Nonnull
  public String name() {
    return name;
  }

  @Nonnull
  public Iterable<String> values() {
    return values;
  }

  @Nullable
  public String lastValue() {
    return
      !values.isEmpty()
        ? values.get(values.size() - 1)
        : null;

  }

  @Override
  public String toString() {
    StringJoiner values = new StringJoiner(", ");
    for (String value : this.values)
      values.add(value);
    return name + ": " + values;
  }
}
