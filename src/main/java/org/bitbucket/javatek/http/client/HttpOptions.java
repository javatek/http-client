package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;
import java.time.Duration;

/**
 *
 */
public interface HttpOptions {
  HttpOptions DEFAULT_OPTIONS = new HttpOptions() {};

  /**
   * @return connection and read timeout for following requests
   */
  @Nonnull
  default Duration timeout() {
    return Duration.ofSeconds(30);
  }
}
