package org.bitbucket.javatek.http.client;

import javax.annotation.Nonnull;

import static org.bitbucket.javatek.require.StringRequires.requireNonEmpty;

/**
 * todo rename to HttpField
 */
public final class FormField {
  private final String name;
  private final String value;

  public FormField(@Nonnull String name, String value) {
    this.name = requireNonEmpty(name);
    this.value = value != null ? value : "";
  }

  @Nonnull
  public String name() {
    return name;
  }

  @Nonnull
  public String value() {
    return value;
  }

  @Override
  public String toString() {
    return name + ": " + value;
  }
}
